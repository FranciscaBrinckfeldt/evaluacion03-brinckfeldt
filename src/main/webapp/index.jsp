<%-- 
    Document   : index.jsp
    Created on : 04-05-2020, 20:01:17
    Author     : Francisca
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
        <link rel="stylesheet" type="text/css" href="style.css">
        <title>Stock de Productos</title>
    </head>
    <body>
        <div class="generalcontainer">
            <h4 class="title is-4">Taller de Aplicaciones Empresariales</h4>

            <div class="content">
                <p><b>Alumna</b>: Francisca Brinckfeldt Fernández.</p>
                <p><b>Sección</b>: 50.</p>
                <p><b>Profesor</b>: César Cruces.</p>
            </div>

            <h4 class="title is-4">Stock de Productos</h4>

            <div class="content">
                <p><b>Método GET</b>: Permite mostrar todos los productos.<br>
                <b>URI</b>: /api/productos</p>
                <p><b>Método GET</b>: Permite buscar un producto por su ID.<br>
                <b>URI</b>: /api/productos/{id}</p>
                <p><b>Método POST</b>: Permite crear un nuevo producto.<br>
                <b>URI</b>: /api/productos</p>
                <p><b>Método PUT</b>: Permite actualizar las características de un producto.<br>
                <b>URI</b>: /api/productos</p>
                <p><b>Método DELETE</b>: Permite eliminar un producto por su ID.<br>
                <b>URI</b>: /api/productos/{id}</p>
            </div>
        </div>
    </body>
</html>
