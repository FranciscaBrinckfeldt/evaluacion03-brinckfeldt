/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.services;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.dao.ProductosJpaController;
import root.persistence.entities.Productos;

/**
 *
 * @author Francisca
 */

@Path("/productos")
public class ProductosRest {
    
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("root.heroku_evaluacion03-brinckfeldt_war_1PU");
    EntityManager em;
    ProductosJpaController dao = new ProductosJpaController();
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarTodo() {
        List<Productos> lista = dao.findProductosEntities();
        return Response.ok(200).entity(lista).build();
        /*em = emf.createEntityManager();
        List<Productos> lista = em.createNamedQuery("Productos.findAll").getResultList();
        return Response.ok(200).entity(lista).build();*/
    }
    
    @GET
    @Path("/{idproducto}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscarId(@PathParam("idproducto") int idProducto) {
        Productos producto = dao.findProductos(idProducto);
        return Response.ok(200).entity(producto).build();
        /*System.out.println(idProducto);
        em = emf.createEntityManager();
        Productos producto = em.find(Productos.class, idProducto);
        return Response.ok(200).entity(producto).build();*/
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response crear(Productos producto) throws Exception {
        dao.create(producto);
        return Response.ok(201).entity(producto).build();
        /*em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(producto);
        em.getTransaction().commit();
        return Response.ok(201).entity(producto).build();*/
    }
    
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response actualizar(Productos producto) throws Exception {
        dao.edit(producto);
        return Response.ok(201).entity(producto).build();
        /*em = emf.createEntityManager();
        em.getTransaction().begin();
        producto = em.merge(producto);
        em.getTransaction().commit();
        return Response.ok(201).entity(producto).build();*/
    }
    
    @DELETE
    @Path("/{idproducto}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response eliminar(@PathParam("idproducto") int idProducto) throws Exception {
        dao.destroy(idProducto);
        return Response.ok("Producto eliminado").build();
        /*em = emf.createEntityManager();
        em.getTransaction().begin();
        Productos producto = em.getReference(Productos.class, idProducto);
        em.remove(producto);
        em.getTransaction().commit();
        return Response.ok(200).entity(producto).build();*/
    }
}
